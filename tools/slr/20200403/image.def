Bootstrap: docker
From: ubuntu:14.04

%post
	# Install system dependencies
	apt-get update
	apt-get install -y wget git build-essential zlib1g-dev

  # Install miniconda
  cd /tmp
  wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
  chmod +x Miniconda3-latest-Linux-x86_64.sh
  ./Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda3
  rm Miniconda3-latest-Linux-x86_64.sh

  export PATH=/opt/miniconda3/bin:$PATH

  # Install BWA, Samtools and Bamtools
  conda install -y -c bioconda bwa=0.7.17
  conda install -y -c bioconda samtools=1.7
  conda install -y -c bioconda bamtools=2.4.0

  export BAMTOOLS_HOME_INCLUDE=/opt/miniconda3/include/bamtools
  export BAMTOOLS_HOME_LIB=/opt/miniconda3/lib

  # Download and install SLR
  cd /opt
  git clone https://github.com/luojunwei/SLR.git
  cd SLR
  make all

%test
  export PATH=/opt/miniconda3/bin:$PATH
  export BAMTOOLS_HOME_INCLUDE=/opt/miniconda3/include/bamtools
  export BAMTOOLS_HOME_LIB=/opt/miniconda3/lib
  export PATH=/opt/SLR:$PATH
  

%environment
  export PATH=/opt/miniconda3/bin:$PATH
  export BAMTOOLS_HOME_INCLUDE=/opt/miniconda3/include/bamtools
  export BAMTOOLS_HOME_LIB=/opt/miniconda3/lib
  export PATH=/opt/SLR:$PATH
  which SLR | grep "/opt/SLR/SLR"

%labels
  Author Julien Seiler <seilerj@igbmc.fr>