Bootstrap: docker
From: ubuntu:20.04

%labels
    Author IFB
    Version master

%environment
  export PATH=/opt/TEtools:/opt/conda/bin:$PATH

%post
    export PATH=/opt/TEtools:/opt/conda/bin:$PATH
    export DEBIAN_FRONTEND=noninteractive
    # prepare debian/ubuntu
    apt-get update && apt-get upgrade -y
    apt-get install -y  \
            apt-utils \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg-agent \
            software-properties-common \
            sed \
            wget \
            git \
            build-essential \
            python3 python3-pip
    apt-get clean
    apt-get purge


    # install conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
    # install dependency
    conda update -y conda
    . /opt/conda/etc/profile.d/conda.sh
    conda install -c conda-forge -y mamba
    mamba install -y -c bioconda -c conda-forge -c defaults\
          "python>=3.6" \
          "r-base>=3.3" \
          r-ggplot2 \
          r-gplots \
          r-rcolorbrewer \
          r-extrafont \
          bioconductor-biobase \
          bioconductor-deseq2 \
          bowtie \
          bowtie2


    # get TEtools
    git clone https://github.com/l-modolo/TEtools.git /opt/TEtools
    chmod +x /opt/TEtools/TEcount.py
    chmod +x /opt/TEtools/TEdiff.R
    cd /opt/TEtools/
    sed -i.bak '1 i #!/opt/conda/bin/python' TEcount.py
    sed -i.bak '1 i #!/opt/conda/bin/Rscript' TEdiff.R


%runscript
    . /opt/conda/etc/profile.d/conda.sh
    exec TEcount.py "$@"

%test
    export PATH=/opt/TEtools:/opt/conda/bin:$PATH
    . /opt/conda/etc/profile.d/conda.sh
    TEcount.py -h
    TEcount.py -version
    # TEdiff.R --help